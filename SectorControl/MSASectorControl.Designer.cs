﻿
namespace SectorControl
{
   partial class MSASectorControl
   {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.autoDrawGroupBox = new System.Windows.Forms.GroupBox();
         this.numberOfSectorsLabel = new System.Windows.Forms.Label();
         this.drawSectorButton = new System.Windows.Forms.Button();
         this.numberOfSectorsTextBox = new System.Windows.Forms.TextBox();
         this.startAngleLabel = new System.Windows.Forms.Label();
         this.autoStartAngleTextBox = new System.Windows.Forms.TextBox();
         this.deleteSectorButton = new System.Windows.Forms.Button();
         this.addSectorButton = new System.Windows.Forms.Button();
         this.nsaLabel = new System.Windows.Forms.Label();
         this.msaTextBox = new System.Windows.Forms.TextBox();
         this.radiusLabel = new System.Windows.Forms.Label();
         this.raduisTextBox = new System.Windows.Forms.TextBox();
         this.manualDrawGroupBox = new System.Windows.Forms.GroupBox();
         this.autoDrawGroupBox.SuspendLayout();
         this.manualDrawGroupBox.SuspendLayout();
         this.SuspendLayout();
         // 
         // autoDrawGroupBox
         // 
         this.autoDrawGroupBox.Controls.Add(this.numberOfSectorsLabel);
         this.autoDrawGroupBox.Controls.Add(this.drawSectorButton);
         this.autoDrawGroupBox.Controls.Add(this.numberOfSectorsTextBox);
         this.autoDrawGroupBox.Controls.Add(this.startAngleLabel);
         this.autoDrawGroupBox.Controls.Add(this.autoStartAngleTextBox);
         this.autoDrawGroupBox.Location = new System.Drawing.Point(5, 305);
         this.autoDrawGroupBox.Name = "autoDrawGroupBox";
         this.autoDrawGroupBox.Size = new System.Drawing.Size(293, 105);
         this.autoDrawGroupBox.TabIndex = 0;
         this.autoDrawGroupBox.TabStop = false;
         this.autoDrawGroupBox.Text = "Auto Draw";
         // 
         // numberOfSectorsLabel
         // 
         this.numberOfSectorsLabel.AutoSize = true;
         this.numberOfSectorsLabel.Location = new System.Drawing.Point(37, 47);
         this.numberOfSectorsLabel.Name = "numberOfSectorsLabel";
         this.numberOfSectorsLabel.Size = new System.Drawing.Size(95, 13);
         this.numberOfSectorsLabel.TabIndex = 3;
         this.numberOfSectorsLabel.Text = "Number of Sectors";
         // 
         // drawSectorButton
         // 
         this.drawSectorButton.Location = new System.Drawing.Point(96, 70);
         this.drawSectorButton.Name = "drawSectorButton";
         this.drawSectorButton.Size = new System.Drawing.Size(91, 23);
         this.drawSectorButton.TabIndex = 1;
         this.drawSectorButton.Text = "Draw Sectors";
         this.drawSectorButton.UseVisualStyleBackColor = true;
         this.drawSectorButton.Click += new System.EventHandler(this.drawSectorButton_Click);
         // 
         // numberOfSectorsTextBox
         // 
         this.numberOfSectorsTextBox.Location = new System.Drawing.Point(155, 44);
         this.numberOfSectorsTextBox.Name = "numberOfSectorsTextBox";
         this.numberOfSectorsTextBox.Size = new System.Drawing.Size(100, 20);
         this.numberOfSectorsTextBox.TabIndex = 2;
         this.numberOfSectorsTextBox.Text = "1";
         // 
         // startAngleLabel
         // 
         this.startAngleLabel.AutoSize = true;
         this.startAngleLabel.Location = new System.Drawing.Point(37, 23);
         this.startAngleLabel.Name = "startAngleLabel";
         this.startAngleLabel.Size = new System.Drawing.Size(86, 13);
         this.startAngleLabel.TabIndex = 1;
         this.startAngleLabel.Text = "Start Angle (deg)";
         // 
         // autoStartAngleTextBox
         // 
         this.autoStartAngleTextBox.Location = new System.Drawing.Point(155, 20);
         this.autoStartAngleTextBox.Name = "autoStartAngleTextBox";
         this.autoStartAngleTextBox.Size = new System.Drawing.Size(100, 20);
         this.autoStartAngleTextBox.TabIndex = 0;
         this.autoStartAngleTextBox.Text = "0";
         // 
         // deleteSectorButton
         // 
         this.deleteSectorButton.Location = new System.Drawing.Point(180, 76);
         this.deleteSectorButton.Name = "deleteSectorButton";
         this.deleteSectorButton.Size = new System.Drawing.Size(75, 23);
         this.deleteSectorButton.TabIndex = 9;
         this.deleteSectorButton.Text = "Delete Sector";
         this.deleteSectorButton.UseVisualStyleBackColor = true;
         // 
         // addSectorButton
         // 
         this.addSectorButton.Location = new System.Drawing.Point(40, 76);
         this.addSectorButton.Name = "addSectorButton";
         this.addSectorButton.Size = new System.Drawing.Size(75, 23);
         this.addSectorButton.TabIndex = 8;
         this.addSectorButton.Text = "Add Sector";
         this.addSectorButton.UseVisualStyleBackColor = true;
         // 
         // nsaLabel
         // 
         this.nsaLabel.AutoSize = true;
         this.nsaLabel.Location = new System.Drawing.Point(37, 53);
         this.nsaLabel.Name = "nsaLabel";
         this.nsaLabel.Size = new System.Drawing.Size(47, 13);
         this.nsaLabel.TabIndex = 7;
         this.nsaLabel.Text = "MSA (m)";
         // 
         // msaTextBox
         // 
         this.msaTextBox.Location = new System.Drawing.Point(155, 50);
         this.msaTextBox.Name = "msaTextBox";
         this.msaTextBox.Size = new System.Drawing.Size(100, 20);
         this.msaTextBox.TabIndex = 6;
         // 
         // radiusLabel
         // 
         this.radiusLabel.AutoSize = true;
         this.radiusLabel.Location = new System.Drawing.Point(37, 26);
         this.radiusLabel.Name = "radiusLabel";
         this.radiusLabel.Size = new System.Drawing.Size(66, 13);
         this.radiusLabel.TabIndex = 5;
         this.radiusLabel.Text = "Radius (NM)";
         // 
         // raduisTextBox
         // 
         this.raduisTextBox.Location = new System.Drawing.Point(155, 23);
         this.raduisTextBox.Name = "raduisTextBox";
         this.raduisTextBox.Size = new System.Drawing.Size(100, 20);
         this.raduisTextBox.TabIndex = 4;
         // 
         // manualDrawGroupBox
         // 
         this.manualDrawGroupBox.Controls.Add(this.deleteSectorButton);
         this.manualDrawGroupBox.Controls.Add(this.addSectorButton);
         this.manualDrawGroupBox.Controls.Add(this.raduisTextBox);
         this.manualDrawGroupBox.Controls.Add(this.nsaLabel);
         this.manualDrawGroupBox.Controls.Add(this.radiusLabel);
         this.manualDrawGroupBox.Controls.Add(this.msaTextBox);
         this.manualDrawGroupBox.Location = new System.Drawing.Point(5, 416);
         this.manualDrawGroupBox.Name = "manualDrawGroupBox";
         this.manualDrawGroupBox.Size = new System.Drawing.Size(292, 105);
         this.manualDrawGroupBox.TabIndex = 2;
         this.manualDrawGroupBox.TabStop = false;
         this.manualDrawGroupBox.Text = "Manual Draw";
         // 
         // MSASectorControl
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.manualDrawGroupBox);
         this.Controls.Add(this.autoDrawGroupBox);
         this.Name = "MSASectorControl";
         this.Size = new System.Drawing.Size(300, 529);
         this.Click += new System.EventHandler(this.MSASectorControl_Click);
         this.autoDrawGroupBox.ResumeLayout(false);
         this.autoDrawGroupBox.PerformLayout();
         this.manualDrawGroupBox.ResumeLayout(false);
         this.manualDrawGroupBox.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox autoDrawGroupBox;
      private System.Windows.Forms.TextBox autoStartAngleTextBox;
      private System.Windows.Forms.Button deleteSectorButton;
      private System.Windows.Forms.Button addSectorButton;
      private System.Windows.Forms.Label nsaLabel;
      private System.Windows.Forms.TextBox msaTextBox;
      private System.Windows.Forms.Label radiusLabel;
      private System.Windows.Forms.TextBox raduisTextBox;
      private System.Windows.Forms.Label numberOfSectorsLabel;
      private System.Windows.Forms.TextBox numberOfSectorsTextBox;
      private System.Windows.Forms.Label startAngleLabel;
      private System.Windows.Forms.Button drawSectorButton;
      private System.Windows.Forms.GroupBox manualDrawGroupBox;
   }
}
