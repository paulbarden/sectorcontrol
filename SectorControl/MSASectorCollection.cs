﻿using System.Collections.Generic;
using System.Drawing;

namespace SectorControl
{
   /// <summary>
   /// All of the MSA sectors to be analyzed
   /// </summary>
   public class MSASectorCollection
   {
      //Default MSA radius is 25 NM
      private const float cDefaultRadius = 25.0f;

      public List<MSASector> Sectors { get; private set; }
      public Point SectorCentre { get; set; }

      /// <summary>
      /// Initialises with four sectors at compass points
      /// </summary>
      /// <param name="sectorCentre"></param>
      public MSASectorCollection(Point sectorCentre)
      {
         SectorCentre = sectorCentre;
         Sectors = new List<MSASector>()
         {
            new MSASector(SectorCentre, 0.0f, 360.0f, cDefaultRadius),
         };
      }

      public MSASectorCollection(Point sectorCentre, int numSectors, int startAngle = 0)
      {
         SectorCentre = sectorCentre;
         int StartAngle = startAngle;

         //Calculate sweep angles
         int Sweep = 360 / numSectors;
         int mod = 360 % numSectors;

         Sectors = new List<MSASector>();

         if (mod == 0)
         {
            for (int i = 0; i < numSectors; i++)
            {
               Sectors.Add(new MSASector(SectorCentre, (float)StartAngle, (float)Sweep, cDefaultRadius));
               StartAngle += Sweep;
            }
         }
         else
         {
            //If we dont get an equal number of degrees per sector, then distribute as evenly as possible.
            int numItems = 360;
            int itemsPerBucket = (numItems / numSectors);
            int remainingItems = (numItems % numSectors);
            for (int i = 1; i <= numSectors; i++)
            {
               int extra = (i <= remainingItems) ? 1 : 0;
               Sweep = itemsPerBucket + extra;
               Sectors.Add(new MSASector(SectorCentre, (float)StartAngle, (float)Sweep, cDefaultRadius));
               StartAngle += Sweep;
            }

         }
      }

      /// <summary>
      /// Handy function that distributes items equally amongst buckets including distrubtion of the remainder.
      /// Keep here for testing.
      /// </summary>
      /// <param name="numBuckets">The number of buckets to distribute the items amongst.</param>
      /// <param name="numItems">The number of items to distribute.</param>
      private void Distribute(int numBuckets, int numItems)
      {
         int itemsPerBucket = (numItems / numBuckets);
         int remainingItems = (numItems % numBuckets);
         int total = 0;

         for (int i = 1; i <= numBuckets; i++)
         {
            int extra = (i <= remainingItems) ? 1 : 0;
            System.Console.WriteLine("bucket " + i + " contains " + (itemsPerBucket + extra) + " items.");
            total += itemsPerBucket + extra;
         }
         System.Console.WriteLine("Total is " + total + "  degrees.");
      }

   }
}
