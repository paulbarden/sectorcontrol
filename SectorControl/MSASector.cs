﻿using System.Collections.Generic;
using System.Drawing;

namespace SectorControl
{
   /// <summary>
   /// A single MSA sector.
   /// </summary>
   public class MSASector
   {
      public Point SectorCentre { get; private set; }
      private float _startAngle;
      public float StartAngle {
         get
         {
            return _startAngle;
         }
         set 
         {
            _startAngle = value - 90;
         } 
      }
      public float SweepAngle { get; set; }
      public float Radius { get; set; }
      public float MSA { get; set; }

      public MSASector(Point sectorCentre)
      {
         SectorCentre = sectorCentre;
         StartAngle = 0.0f;
         SweepAngle = 0.0f;
         Radius = 0.0f;
         MSA = 0.0f;
      }

      public MSASector(Point sectorCentre, float start, float sweep, float radius)
      {
         SectorCentre = sectorCentre;
         StartAngle = start;
         SweepAngle = sweep;
         Radius = radius;
         MSA = 0.0f;
      }
   }

}
