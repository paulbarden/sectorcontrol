﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SectorControl
{
   public partial class MSASectorControl : UserControl
   {
      private Graphics _sectorGraphics;
      private Point _sectorCentre = new Point(150, 150);
      private int _sectorDiameter = 290;
      private int _sectorOffset = 5;

      private MSASectorCollection _sectors;

      public MSASectorControl()
      {
         InitializeComponent();
         this.DoubleBuffered = true;
         _sectors = new MSASectorCollection(_sectorCentre);
         _sectorGraphics = this.CreateGraphics();
      }

      public void InitialiseGraphics()
      {
         Pen p = new Pen(Color.Blue);

         _sectorGraphics.Clear(this.BackColor);
         foreach (MSASector sector in _sectors.Sectors)
         {
            _sectorGraphics.DrawPie(p, _sectorOffset, _sectorOffset, _sectorDiameter, _sectorDiameter, sector.StartAngle, sector.SweepAngle);
         }
      }

      private void drawSectorButton_Click(object sender, System.EventArgs e)
      {

         if (!int.TryParse(numberOfSectorsTextBox.Text, out int numberOfSectors))
         {
            MessageBox.Show("Number Of Sectors must be a positive integer", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
         }
         if (!int.TryParse(autoStartAngleTextBox.Text, out int autoStartAngle))
         {
            MessageBox.Show("Start Angle must be an integer", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
         }

         _sectors = new MSASectorCollection(_sectorCentre, numberOfSectors, autoStartAngle);
         InitialiseGraphics();
         _sectorGraphics = this.CreateGraphics();
      }

      private void MSASectorControl_Click(object sender, System.EventArgs e)
      {
         MouseEventArgs mev = e as MouseEventArgs;
         SolidBrush b = new SolidBrush(Color.LightBlue);

         InitialiseGraphics();
         _sectorGraphics = this.CreateGraphics();

         MSASector sector = clickedSector(mev.X, mev.Y);
         if(sector != null)
            _sectorGraphics.FillPie(b, _sectorOffset, _sectorOffset, _sectorDiameter, _sectorDiameter, sector.StartAngle, sector.SweepAngle);
      }

      private MSASector clickedSector(double x, double y)
      {
         double theta = 0.0;
         //Calculate distance between points and if less than radius continue
         if ((System.Math.Pow(x - _sectorCentre.X, 2) + System.Math.Pow(y - _sectorCentre.Y, 2)) < System.Math.Pow( (double)(_sectorDiameter/2.0) , 2))
         {
            //Calculate angle from point defined by x, y to the centre.
            double delta_x = x - _sectorCentre.X;
            double delta_y = y - _sectorCentre.Y;
            theta = (System.Math.Atan2(delta_y, delta_x) * (180 / System.Math.PI));
            theta = CorrectAngle(theta);
            //theta = (System.Math.Sign(theta) == -1) ? theta + 360.0 : theta;
            //theta = (theta + 90.0) % 360.0;
         }

         //We know the angle so find which sector it is in.
         foreach (MSASector sector in _sectors.Sectors)
         {
            if (theta >= CorrectAngle(sector.StartAngle) && theta <= (CorrectAngle(sector.StartAngle) + sector.SweepAngle))
               return sector;
         }

         MessageBox.Show("Could not find the sector.", "Sector not found", MessageBoxButtons.OK, MessageBoxIcon.Error);

         return null; 
      }

      private double CorrectAngle(double theta)
      {
         theta = (System.Math.Sign(theta) == -1) ? theta + 360.0 : theta;
         theta = (theta + 90.0) % 360.0;
         return theta;
      }

   }
}
