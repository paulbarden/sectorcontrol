﻿namespace SectorControl
{
   partial class MSAform
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.msaSectorControl = new SectorControl.MSASectorControl();
         this.SuspendLayout();
         // 
         // msaSectorControl
         // 
         this.msaSectorControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.msaSectorControl.Location = new System.Drawing.Point(30, 32);
         this.msaSectorControl.Name = "msaSectorControl";
         this.msaSectorControl.Size = new System.Drawing.Size(316, 542);
         this.msaSectorControl.TabIndex = 0;
         // 
         // MSAform
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(386, 586);
         this.Controls.Add(this.msaSectorControl);
         this.Name = "MSAform";
         this.Text = "Minimum Sector Altitudes";
         this.ResumeLayout(false);

      }

      #endregion

      private MSASectorControl msaSectorControl;
   }
}

